/**
 * 将 dist 目录更新到 cordova 项目的目录中
 */

const program = require('commander')
const path = require('path')
const shell = require('shelljs')

let cwd = process.cwd()
let sourceImgDir = path.resolve(cwd, './dist/img/')
let sourceJsDir = path.resolve(cwd, './dist/js/')
let sourceCssDir = path.resolve(cwd, './dist/css/')
let sourceFontsDir = path.resolve(cwd, './dist/fonts/')

program.option('-p, --platform', 'ios/android')
       .option('-t, --target', 'player/coach')
       .action((platform, target) => {
          console.log(`platform is ${platform}`)
          console.log(`target is ${target}`)
          let targetDir = path.resolve(cwd, `./../sport-${target}`)
          let rootDir = path.resolve(targetDir, './www/')
          let platformDir = path.resolve(targetDir, `./merges/${platform}`)

          let jsFiles = ['manifest.js', 'vendor.js', `${target}.js`]
          let cssFiles = [`${target}.css`]
          let imgs = ['*.png', '*.jpg']
          let fonts = ['iconfont.eot', 'iconfont.svg', 'iconfont.ttf', 'iconfont.woff']

          // copy(jsFiles, sourceJsDir, path.resolve(platformDir, './www/js/'))
          // copy(cssFiles, sourceCssDir, path.resolve(platformDir, './www/css/'))
          // copy(fonts, sourceFontsDir, path.resolve(platformDir, './www/fonts/'))
          // copy(imgs, sourceImgDir, path.resolve(platformDir, './www/img/'))

          copy(jsFiles, sourceJsDir, path.resolve(rootDir, './js/'))
          copy(cssFiles, sourceCssDir, path.resolve(rootDir, './css/'))
          copy(fonts, sourceFontsDir, path.resolve(rootDir, './fonts/'))
          copy(imgs, sourceImgDir, path.resolve(rootDir, './img/'))
       })

// helper
function copy(files, sourceDir, distDir) {
  files.forEach((file) => {
    let source = path.resolve(sourceDir, `./${file}`)
    console.log(`copy file ${source} to ${distDir}`)
    shell.cp(source, distDir)
  })
}

program.parse(process.argv)
