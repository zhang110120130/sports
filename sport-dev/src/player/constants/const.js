/**
 * 一些常量
 */

export const PULSE_MIN = 20
export const PULSE_MAX = 70
export const PULSE_RANGE = range(PULSE_MIN, PULSE_MAX)

// helper
function range(min, max) {
  let _range = Array.from(Array(max - min + 1), (_, x) => {
    return min + x
  })
  return _range
}
