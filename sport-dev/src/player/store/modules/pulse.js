/**
 * record pulse
 */
import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { SUBMIT_PULSE, SUBMIT_PULSE_SUCCESS, SUBMIT_PULSE_FAIL } from 'player/constants/action-types'
import { API_PULSE } from 'player/constants/api'
import createGetters from 'common/utils/create-getters'

const state = {
  pulseSubmitStatus: DEFAULT,
  pulseMessage: ''
}

const getters = createGetters(state)

const actions = {
  submitPulse ({commit, state}, data) {
    console.log('submitPulse', data)
    commit(SUBMIT_PULSE, null)
    fetch.post(API_PULSE, data).then((res) => {
      if (parseInt(res.code,10) === 1) {
        commit(SUBMIT_PULSE_SUCCESS, res)
      } else {
        let message = res.message
        if (parseInt(res.code,10) === 2) {
          if (data.pulseType === 'MORNING') {
            message = '晨脉数据已提交, 请勿重复提交'
          } else {
            message = '晚脉数据已提交,  请勿重复提交'
          }
        }
        commit(SUBMIT_PULSE_FAIL, message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [SUBMIT_PULSE] (state) {
    state.pulseSubmitStatus = PENDING
    state.pulseMessage = ''
  },

  [SUBMIT_PULSE_FAIL] (state, message) {
    state.pulseSubmitStatus = FAIL
    state.pulseMessage = message
  },

  [SUBMIT_PULSE_SUCCESS] (state, res) {
    state.pulseSubmitStatus = SUCCESS
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}