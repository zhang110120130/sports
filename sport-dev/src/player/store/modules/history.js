import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOAD_HISTORY_LIST, LOAD_HISTORY_LIST_SUCCESS, LOAD_HISTORY_LIST_FAIL, LOAD_DIARY_REPLIED_FAIL, LOAD_DIARY, LOAD_DIARY_FAIL, LOAD_DIARY_SUCCESS
       } from 'player/constants/action-types'
import { API_DIARY_HISTORY, API_DIARY_DETAIL } from 'player/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  historyLoadStatus: DEFAULT,
  // 历史数据列表
  historyInited: false,
  historyTotal: 0,
  historyList: [],
  historyLoadMessage: '',
  historyLoadQuery: {
    pageSize: 5,
    pageNo: 1
  }
}

const getters = createGetters(state)

const actions = {
  /**
   * query
   * @param {pageSize} 5
   * @param {pageNo} 1
   */
  loadHistoryList ({commit, state}, query) {
    commit(LOAD_HISTORY_LIST)
    let user = StorageMaster.getItem('user')
    let _query = Object.assign({}, query, {
      playerId: user.id
    })
    fetch.get(API_DIARY_HISTORY, _query).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_HISTORY_LIST_SUCCESS, res.data)
      } else {
        commit(LOAD_HISTORY_LIST_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}

const mutations = {
  [LOAD_HISTORY_LIST] (state, query) {
    state.historyLoadStatus = PENDING
    state.historyLoadMessage = ''
  },
  [LOAD_HISTORY_LIST_FAIL] (state, message) {
    state.historyLoadStatus = FAIL
    state.historyLoadMessage = message
  },
  [LOAD_HISTORY_LIST_SUCCESS] (state, data) {
    let list = state.historyList
    // let filted_list = data.content.filter((item) => {
    //   let existed = list.filter((_item) => {
    //     return _item.diary.id === item.diary.id
    //   })[0]
    //   return !existed
    // })

    data.content.forEach((item) => {
      let existed = list.filter((_item) => {
        return _item.diary.id === item.diary.id
      })[0]
      if (!existed) {
        list.push(item)
      }
    })

    // state.historyList = list
    state.historyTotal = data.totalElements
    state.historyLoadStatus = SUCCESS
    state.historyInited = true
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}