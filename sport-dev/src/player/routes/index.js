import Vue from 'vue'
import PlayerHomepage from 'player/views/homepage'
import PlayerHistory from 'player/views/history'
import PlayerDiary from 'player/views/diary'
import PlayerDiaryDetail from 'player/views/diary-detail'
import PlayerPulse from 'player/views/pulse'
import PlayerDiaryWrite from 'player/views/diary-write'
import PlayerProfileEdit from 'player/views/profile-edit'
import PlayerSelectCoach from 'player/views/select-coach'
import News from 'player/views/news'
import Analeptic from 'player/views/analeptic'
import Result from 'player/views/result'

import About from 'common/views/about'
import Password from 'common/views/password'
import Help from 'common/views/help'
import Feedback from 'common/views/feedback'
import Login from 'common/views/login'
import Logout from 'common/views/logout'
import User from 'common/views/user'
import Success from 'common/views/success'
import Playerreply from 'common/views/playerreply'

const routes = [
  {
    path: '*', 
    redirect: '/profile/edit'
  },
  {
    path: '/homepage',
    name: 'homepage',
    component: PlayerHomepage
  },
  {
    path: '/history',
    name: 'history',
    component: PlayerHistory
  },
  {
    path: '/diary',
    name: 'diary',
    component: PlayerDiary
  },
  {
    path: '/diary/detail/:id',
    name: 'diary-detail',
    component: PlayerDiaryDetail
  },
  {
    path: '/diary/write',
    name: 'diary-write',
    component: PlayerDiaryWrite
  },
  {
    path: '/pulse',
    name: 'pulse',
    component: PlayerPulse
  },
  {
    path: '/profile/edit',
    name: 'profile-edit',
    component: PlayerProfileEdit
  },
  {
    path: '/select/coach/:id/:gender',
    name: 'select-coach',
    component: PlayerSelectCoach
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/password',
    name: 'passwrd',
    component: Password
  },
  {
    path: '/help',
    name: 'help',
    component: Help
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: Feedback
  },
  {
    path: '/user',
    name: 'user',
    component: User
  },
  {
    path: '/success',
    name: 'success',
    component: Success
  },
  {
    path: '/news',
    name: 'news',
    component: News
  },
  {
    path: '/analeptic',
    name: 'analeptic',
    component: Analeptic
  },
  {
    path: '/result',
    name: 'result',
    component: Result
  },
  {
    path: '/playerreply/:id',
    name: 'playerreply',
    component: Playerreply
  }
]

export default routes