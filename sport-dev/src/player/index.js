import 'mint-ui/lib/style.css'

import Vue from 'vue'
import routes from './routes'
import App from './views/App'
import Router from 'vue-router'
import store from './store'
import { sync } from 'vuex-router-sync'
import { StorageInit, StorageMaster } from 'common/utils/storage'
import MintUI from 'mint-ui'
import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
import syncDict from 'common/utils/sync-dict'
import registeredGlobal from 'common/utils/registered-global'
import { routerLoginCheck } from 'common/utils/permission-check'
import DictInit from 'common/utils/dict-init'

const INIT_DATA = {
  setting: {
    role: 'player'
  }
}

const APP = {
  init: () => {
    StorageInit(INIT_DATA)

    Vue.use(Router)
    Vue.use(MintUI)
    Vue.use(preview)

    let router = new Router({
      mode: 'history',
      routes
    })

    // sync store and router
    sync(store, router)

    // router check login
    routerLoginCheck(router)

    let VueApp = new Vue({
      router,
      store,
      components: { App },
      template: '<App />',
    })

    DictInit({
      callback: () => {
        VueApp.$mount('#app')
        registeredGlobal()
        syncDict()
      }
    })
  }
}

window.APP = APP
