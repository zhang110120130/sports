/**
 * cordova upload file
 * 依赖 cordova-plugin-file-transfer
 */

import { API_SERVER } from 'common/constants/const'


const DEFAULT_API = '/v1/file/uploadSingle'

export default (fileURL, params, url=`${API_SERVER}${DEFAULT_API}`) => {
  let options = new FileUploadOptions()
  options.fileKey = 'file'
  options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1)
  options.mimeType = "image/jpeg"
  options.chunkedMode = false

  let _params = Object.assign({}, params)
  options.params = _params
 
  return new Promise((resolve, reject) => {
    let ft = new FileTransfer()
    ft.upload(fileURL, encodeURI(url), function (res) {
      resolve(res)
    }, function (err) {
      reject(err)
    } , options, true)
  })

}