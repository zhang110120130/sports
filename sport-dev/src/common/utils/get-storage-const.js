/**
 * 获取一些通用的存储在 storage 中的数据
 * 可取到的数据有
 * traningVolume
 * trainingIntensity
 * 
 */

import { StorageMaster } from './storage'
import { list2dict, trans2const } from './helper'

export default (keys) => {
  let r = {}
  keys.forEach((key) => {
    let data = StorageMaster.getItem(key)
    if (data) {
      let dict_name = trans2const(key, '', '_DICT')
      let list_name = trans2const(key, '', '_LIST')
      r[list_name] = data
      r[dict_name] = list2dict(data)
    } else {
      console.error(`本地没有存储相关 key 为 ${key} 的数据`)
    }
  })
  return r
}