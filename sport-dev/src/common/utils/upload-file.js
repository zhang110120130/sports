/**
 * cordova 环境中调取相册/相机
 * 选择图片后上传
 * 返回 promise 对象处理上传后的 response
 * 非 cordova 环境暂时不支持
 */

import { getPhoto, takePhoto } from './cordova-photo'
import upload from './cordova-upload'

// let is_cordova = window.cordova

// url: upload file url
// action: camera/album
// photoConf: get/take photo config
// uploadConf: upload file conf
// export default (url, action='camera', photoConf, uploadConf) => {
//   if (!is_cordova) {
//     console.error('[error in upload-file] just support cordova')
//     return false
//   }
//   if (action === 'camera') {
//     return takePhoto(photoConf).then((item) => {
//       return upload(item, url, uploadConf)
//     }).catch((err) => {
//       console.error('[error in upload-file]')
//       console.error(err)
//     })
//   } else if (action === 'album') {
//     return getPhoto(photoConf).then((results) => {
//       let promise_list = results.map((item) => {
//         return upload(item, url, uploadConf)
//       })
//       return new Promise.all(promise_list)
//     }).catch((err) => {
//       console.error('[error in upload-file]')
//       console.error(err)
//     })
//   } else {
//     console.error('[error in upload-file] invalid paramas action')
//   }
// }