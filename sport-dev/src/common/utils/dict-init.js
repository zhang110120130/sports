import { StorageMaster, Storage } from './storage'
import { DICT_LIST, PLACEHOLD_DICT } from 'common/constants/const'

export default ({dict_list=DICT_LIST, callback}) => {
  dict_list.map((k) => {
    let storaged = StorageMaster.getItem(k)
    if (!storaged) {
      StorageMaster.setItem(k, PLACEHOLD_DICT[k])
    }
  })
  callback()
}