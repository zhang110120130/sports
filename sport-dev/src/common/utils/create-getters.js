/**
 * 根据 state 返回 getters
 */
export default (state) => {
  const _getters = {}

  for (let key in state) {
    let name = camelCase(key)
    _getters[key] = (state) => {
      return state[key]
    }
  }

  return _getters
}

// helper
function camelCase(str) {
  return str.replace(/_([a-z]{1})/g, ($1, $2) => {
    return $2.toUpperCase()
  })
}