/**
 * cordova 上传图片
 */
import { getPhoto } from './cordova-photo'
import getFile from './cordova-get-file'
import upload from './cordova-upload'

/**
 * photoConfig:
 * maximumImagesCount: 1,
 * width: 1920,
 * height: 1440,
 * quality: 100
 * 
 * uploadConfig:
 * url: uploadUrl,
 * options:
 *    fileKey: "file"
 *    fileName: fileURL.substr(fileURL.lastIndexOf('/')+1);
 *    file.mimeType: 'image'
 *    headers: {}
 */

export default (photoConfig, uploadConfig) => {
  return getPhoto(photoConfig).then((fileURI) => {
    let uploads = fileURI.map((uri) => {
      return upload(uri, uploadConfig)
    })
    return Promise.all(uploads)
  })
}