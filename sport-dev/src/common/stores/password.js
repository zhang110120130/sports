import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { CHANGE_PASSWORD, CHANGE_PASSWORD_FAIL, CHANGE_PASSWORD_SUCCESS } from 'common/constants/action-types'
import { API_CHANGE_PASSWORD } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'


const state = {
  changePasswordStataus: DEFAULT,
  changePasswordMessage: ''
}

const getters = createGetters(state)

const actions = {
  changePassword ({commit, state}, data) {
    const setting = StorageMaster.getItem('setting')
    commit(CHANGE_PASSWORD, data)
    fetch.post(API_CHANGE_PASSWORD, data, { role: setting.role }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(CHANGE_PASSWORD_SUCCESS, res)
      } else {
        commit(CHANGE_PASSWORD_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  }
}


const mutations = {
  [CHANGE_PASSWORD] (state) {
    state.changePasswordStataus = PENDING
    state.changePasswordMessage = ''
  },
  [CHANGE_PASSWORD_FAIL] (state, message) {
    state.changePasswordStataus = FAIL
    state.changePasswordMessage = message
  },
  [CHANGE_PASSWORD_SUCCESS] (state) {
    state.changePasswordStataus = SUCCESS
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}