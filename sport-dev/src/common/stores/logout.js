import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { LOGOUT, LOGOUT_FAIL, LOGOUT_SUCCESS, LOGIN_FAIL } from 'common/constants/action-types'
import { API_LOGOUT } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

export const state = {
  logoutStatus: DEFAULT,
  logoutMessage: ''
}

export const getters = createGetters(state)


export const actions = {
  logout ({commit, state}) {
    const setting = StorageMaster.getItem('setting')
    const loginName = StorageMaster.getItem('loginName')
    commit(LOGOUT)
    fetch.post(API_LOGOUT, {loginName}, { role: setting.role }).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOGOUT_SUCCESS)
      } else {
        commit(LOGOUT_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOGOUT_FAIL, '网络错误')
    })
  }
}

export const mutations = {
  [LOGOUT] (state) {
    state.logoutStatus = PENDING
  },
  [LOGOUT_SUCCESS] (state) {
    state.logoutStatus = SUCCESS
    StorageMaster.removeItem('user')
    StorageMaster.removeItem('profile')
    StorageMaster.removeItem('loginName')
  },
  [LOGIN_FAIL] (state) {
    state.logoutStatus = FAIL
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}