import Vue from 'vue'
import Vuex from 'vuex'
import routes from 'player/routes'
import App from 'player/views/App'
import Router from 'vue-router'
import store from 'player/stores'

Vue.use(Router)
Vue.use(Vuex)

let router = new Router({
  mode: 'history',
  routes
})

let VueApp = new Vue({
  router,
  store,
  components: { App },
  template: '<App />',
})

VueApp.$mount('#app')