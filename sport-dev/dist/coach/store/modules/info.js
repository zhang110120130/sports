/**
 * 情报相关
 * 包括：
 * 1. 获取历史
 * 2. 提交情报
 */

import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { API_TRAINING_INFO_SUBMIT, API_TRAINING_INFO_HISTORY, API_TRAINING_INFO_DETAIL } from 'coach/constants/api'
import { LOAD_TRAINING_INFO_HISTORY, LOAD_TRAINING_INFO_HISTORY_FAIL,
         LOAD_TRAINING_INFO_HISTORY_SUCCESS, SUBMIT_TRAINING_INFO,
         SUBMIT_TRAINING_INFO_FAIL, SUBMIT_TRAINING_INFO_SUCCESS,
         FLUSH_TRAINING_INFO, LOAD_TRAINING_INFO,
         LOAD_TRAINING_INFO_SUCCESS, LOAD_TRAINING_INFO_FAIL } from 'coach/constants/action-types'
import { StorageMaster } from 'common/utils/storage'
import createGetters from 'common/utils/create-getters'

const state = {
  loadTrainingInfoHistoryStatus: DEFAULT,
  loadTrainingInfoHistoryMessage: '',
  loadTrainingInfoHistoryTotal: 1,
  trainingInfoList: [],
  trainingInfoHistoryQuery: {
    pageSize: 10,
    pageNo: 1
  },
  
  loadTrainingInfoStatus: DEFAULT,
  loadTrainingInfoMessage: '',
  trainingInfo: null,
  
  submitTrainingInfoStatus: DEFAULT,
  submitTrainingInfoMessage: '',
}

const getters = createGetters(state)

const actions = {
  flushTrainingInfo ({commit}) {
    commit(FLUSH_TRAINING_INFO)
  },
  loadTrainingInfoHistory ({commit, state}, query) {
    const user = StorageMaster.getItem('user')
    let _query = Object.assign({}, {
      coachId: user.id
    }, query)
    commit(LOAD_TRAINING_INFO_HISTORY, _query)
    fetch.get(API_TRAINING_INFO_HISTORY, _query).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_TRAINING_INFO_HISTORY_SUCCESS, res.data)
      } else {
        commit(LOAD_TRAINING_INFO_HISTORY_FAIL, res.message)
      }
    }).catch((err) => {
      commit(LOAD_TRAINING_INFO_HISTORY_FAIL, '加载失败, 请重试')
    })
  },
  loadTrainingInfo ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, {
      coachId: user.id
    }, data)
    fetch.get(API_TRAINING_INFO_DETAIL, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_TRAINING_INFO_SUCCESS, res.data)
      } else {
        commit(LOAD_TRAINING_INFO_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_TRAINING_INFO_FAIL, '加载失败, 请重试')
    })
  },
  submitTrainingInfo ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(SUBMIT_TRAINING_INFO)
    fetch.post(API_TRAINING_INFO_SUBMIT, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SUBMIT_TRAINING_INFO_SUCCESS, res.data)
      } else {
        commit(SUBMIT_TRAINING_INFO_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(SUBMIT_TRAINING_INFO_FAIL, '提交失败, 请重试')
    })
  }
}

const mutations = {
  [FLUSH_TRAINING_INFO] (state) {
    state.trainingInfo = null
  },
  [LOAD_TRAINING_INFO_HISTORY] (state) {
    state.loadTrainingInfoHistoryStatus = PENDING
    state.loadTrainingInfoHistoryMessage = ''
  },
  [LOAD_TRAINING_INFO_HISTORY_FAIL] (state, message) {
    state.loadTrainingInfoHistoryStatus = FAIL
    state.loadTrainingInfoHistoryMessage = message
  },
  [LOAD_TRAINING_INFO_HISTORY_SUCCESS] (state, data) {
    let list = [...state.trainingInfoList]
    let filted_list = data.data.filter((item) => {
      let existed = list.find((_item) => {
        return item.id == _item.id
      })
      return !existed
    })
    let _list = filted_list.concat(list)
    state.trainingInfoList = _list
    state.loadTrainingInfoHistoryStatus = SUCCESS
    state.loadTrainingInfoHistoryTotal = data.recordsTotal
  },
  [LOAD_TRAINING_INFO] (state) {
    state.loadTrainingInfoStatus = PENDING
    state.loadTrainingInfoMessage = ''
  },
  [LOAD_TRAINING_INFO_FAIL] (state, message) {
    state.loadTrainingInfoMessage = message
    state.loadTrainingInfoStatus = FAIL
  },
  [LOAD_TRAINING_INFO_SUCCESS] (state, data) {
    state.loadTrainingInfoStatus = SUCCESS
    state.trainingInfo = data
  },
  [SUBMIT_TRAINING_INFO] (state) {
    state.submitTrainingInfoStatus = PENDING
    state.submitTrainingInfoMessage = ''
  },
  [SUBMIT_TRAINING_INFO_FAIL] (state, message) {
    state.submitTrainingInfoStatus = FAIL
    state.submitTrainingInfoMessage = message
  },
  [SUBMIT_TRAINING_INFO_SUCCESS] (state, data) {
    state.submitTrainingInfoStatus = SUCCESS
    state.trainingInfo = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}