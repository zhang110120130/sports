/**
 * 计划相关的
 * 1. 获取历史
 * 2. 发布计划
 */

import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'

import { API_TRAINING_PLAN_HISTORY, 
         API_TRAINING_PLAN_DETAIL, 
         API_TRAINING_PLAN_SUBMIT, 
         API_TRAINING_PLAN_UPDATE} from 'coach/constants/api'
import { LOAD_PLAN_HISTORY, LOAD_PLAN_HISTORY_FAIL,
         LOAD_PLAN_HISTORY_SUCCESS, LOAD_PLAN,
         LOAD_PLAN_FAIL, LOAD_PLAN_SUCCESS,
         SUBMIT_PLAN, SUBMIT_PLAN_FAIL,
         SUBMIT_PLAN_SUCCESS, FLUSH_PLAN, UPDATE_PLAN, 
         UPDATE_PLAN_FAIL, UPDATE_PLAN_SUCCESS } from 'coach/constants/action-types'
import createGetters from 'common/utils/create-getters'
import { StorageMaster } from 'common/utils/storage'

const state = {
  loadPlanHistoryStatus: DEFAULT,
  loadPlanHistoryMessage: '',
  loadPlanHistoryQuery: {
    pageSize: 10,
    pageNo: 1
  },
  loadPlanHistoryTotal: 1,
  planList: [],
  loadPlanStatus: DEFAULT,
  loadPlanMessage: '',
  plan: null,
  submitPlanStatus: DEFAULT,
  submitPlanMessage: '',
  updatePlanStatus: DEFAULT,
  updatePlanMessage: ''
}

const getters = createGetters(state)

const actions = {
  flushPlan ({commit, state}) {
    commit(FLUSH_PLAN)
  },
  loadPlanHistory ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(LOAD_PLAN_HISTORY, _data)
    fetch.get(API_TRAINING_PLAN_HISTORY, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_PLAN_HISTORY_SUCCESS, res.data)
      } else {
        commit(LOAD_PLAN_HISTORY_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_PLAN_HISTORY_FAIL, '网络错误, 请重试')
    })
  },
  loadPlan ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(LOAD_PLAN)
    fetch.get(API_TRAINING_PLAN_DETAIL, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_PLAN_SUCCESS, res.data)
      } else {
        commit(LOAD_PLAN_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOAD_PLAN_FAIL, '网络错误, 请重试')
    })
  },
  submitPlan ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(SUBMIT_PLAN)
    fetch.post(API_TRAINING_PLAN_SUBMIT, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(SUBMIT_PLAN_SUCCESS, res.data)
      } else {
        commit(SUBMIT_PLAN_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(SUBMIT_PLAN_FAIL, '网络错误, 请重试')
    })
  },
  updatePlan ({commit, state}, data) {
    const user = StorageMaster.getItem('user')
    let _data = Object.assign({}, data, {
      coachId: user.id
    })
    commit(UPDATE_PLAN)
    fetch.post(API_TRAINING_PLAN_SUBMIT, _data).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(UPDATE_PLAN_SUCCESS, res.data)
      } else {
        commit(UPDATE_PLAN_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
      commit(UPDATE_PLAN_FAIL, '网络错误, 请重试')
    })
  }
}

const mutations = {
  [FLUSH_PLAN] (state) {
    state.plan = null
    state.loadPlanStatus = DEFAULT
    state.updatePlanStatus = DEFAULT
    state.submitPlanStatus = DEFAULT
  },
  [LOAD_PLAN_HISTORY] (state, query) {
    state.loadPlanHistoryStatus = PENDING
    state.loadPlanHistoryMessage = ''
    state.loadPlanHistoryQuery = query
  },
  [LOAD_PLAN_HISTORY_SUCCESS] (state, data) {
    let list = [...state.planList]
    let filted_list = data.data.filter((item) => {
      let existed = list.find((_item) => {
        return item.plan.id == _item.plan.id
      })
      return !existed
    })
    let _list = filted_list.concat(list)
    state.planList = _list
    state.loadPlanHistoryTotal = data.recordsTotal
    state.loadPlanHistoryStatus = SUCCESS
  },
  [LOAD_PLAN_HISTORY_FAIL] (state, message) {
    state.loadPlanHistoryStatus = FAIL
    state.loadPlanHistoryMessage = message
  },
  [LOAD_PLAN] (state) {
    state.loadPlanStatus = PENDING
  },
  [LOAD_PLAN_FAIL] (state, message) {
    state.loadPlanStatus = FAIL
    state.loadPlanMessage = message
  },
  [LOAD_PLAN_SUCCESS] (state, data) {
    state.loadPlanStatus = SUCCESS
    state.plan = data
  },
  [SUBMIT_PLAN] (sate) {
    state.submitPlanStatus = PENDING
  },
  [SUBMIT_PLAN_FAIL] (state, message) {
    state.submitPlanMessage = message
    state.submitPlanStatus = FAIL
  },
  [SUBMIT_PLAN_SUCCESS] (state, data) {
    state.submitPlanStatus = SUCCESS
    state.plan = data
  },
  [UPDATE_PLAN] (state, data) {
    state.updatePlanStatus = PENDING
  },
  [UPDATE_PLAN_SUCCESS] (state, data) {
    state.updatePlanStatus = SUCCESS
  },
  [UPDATE_PLAN_FAIL] (state, message) {
    state.updatePlanStatus = FAIL
    state.updatePlanMessage = message
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}