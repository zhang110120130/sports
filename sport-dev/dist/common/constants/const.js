/**
 * 一些常量
 */

export const DATE_FORMAT = 'YYYY-MM-DD'

// 需要同步的 dict list
export const DICT_LIST = [
  'coachLevel',
  'completeStatus',
  'evaluationStatus',
  'injuryPart',
  'moodStatus',
  'periodStatus',
  'pulseType',
  'resourceType',
  'teamLevel',
  'trainingIntensity',
  'trainingVolume',
  'trainingContent',
  'trainingRecoveryMethod',
  'bloodType',
  'gender'
]

// placehold dict
export const bloodType = [{"name":"A","value":"A"},{"name":"B","value":"B"},{"name":"O","value":"O"},{"name":"AB","value":"AB"},{"name":"未知","value":"UNKNOWN"}]
export const coachLevel = [{"name":"初级","value":"JUNIOR"},{"name":"中级","value":"MIDDLE"},{"name":"高级","value":"SENIOR"},{"name":"国家级","value":"NATIONAL"}]
export const completeStatus = [{"name":"完成","value":"FINISHED"},{"name":"基本完成","value":"BASIC_COMPLETE"},{"name":"未完成","value":"UNFINISHED"},{"name":"变更","value":"CHANGE"}]
export const evaluationStatus = [{"name":"非常满意","value":"VERY_SATISFY"},{"name":"满意","value":"SATISFY"},{"name":"基本满意","value":"BASIC_SATISFY"},{"name":"不满意","value":"DISSATISFY"}]
export const gender = [{"name":"男","value":"MALE"},{"name":"女","value":"FEMALE"}]
export const injuryPart = [{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877798000,"showOrder":1,"updateTime":1519877799000,"id":14,"version":0,"parentId":1,"bodyPart":{"name":"头部","value":"HEAD"}},{"ROOT":0,"createTime":1519877814000,"showOrder":2,"updateTime":1519877815000,"id":15,"version":0,"parentId":1,"bodyPart":{"name":"颈部","value":"NECK"}}],"createTime":1519877100000,"showOrder":1,"updateTime":1519877101000,"id":1,"version":0,"parentId":0,"bodyPart":{"name":"头部","value":"HEAD_AREA"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877831000,"showOrder":1,"updateTime":1519877832000,"id":16,"version":0,"parentId":2,"bodyPart":{"name":"左肩","value":"LEFT_SHOULDER"}}],"createTime":1519877121000,"showOrder":2,"updateTime":1519877123000,"id":2,"version":0,"parentId":0,"bodyPart":{"name":"左肩部","value":"LEFT_SHOULDER_AREA"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877843000,"showOrder":1,"updateTime":1519877844000,"id":17,"version":0,"parentId":3,"bodyPart":{"name":"右肩","value":"RIGHT_SHOULDER"}}],"createTime":1519877147000,"showOrder":3,"updateTime":1519877147000,"id":3,"version":0,"parentId":0,"bodyPart":{"name":"右肩部","value":"RIGHT_SHOULDER_AREA"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877873000,"showOrder":1,"updateTime":1519877873000,"id":18,"version":0,"parentId":4,"bodyPart":{"name":"胸部","value":"CHEST"}},{"ROOT":0,"createTime":1519877884000,"showOrder":2,"updateTime":1519877884000,"id":19,"version":0,"parentId":4,"bodyPart":{"name":"左背部","value":"LEFT_BACK"}},{"ROOT":0,"createTime":1519877898000,"showOrder":3,"updateTime":1519877898000,"id":20,"version":0,"parentId":4,"bodyPart":{"name":"右背部","value":"RIGHT_BACK"}}],"createTime":1519877185000,"showOrder":4,"updateTime":1519877186000,"id":4,"version":0,"parentId":0,"bodyPart":{"name":"上身","value":"UPPER_BODY"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877925000,"showOrder":1,"updateTime":1519877925000,"id":21,"version":0,"parentId":5,"bodyPart":{"name":"左腰部","value":"LEFT_WAIST"}},{"ROOT":0,"createTime":1519877935000,"showOrder":2,"updateTime":1519877936000,"id":22,"version":0,"parentId":5,"bodyPart":{"name":"右腰部","value":"RIGHT_WAIST"}},{"ROOT":0,"createTime":1519877949000,"showOrder":3,"updateTime":1519877950000,"id":23,"version":0,"parentId":5,"bodyPart":{"name":"肠胃","value":"INTESTINE"}},{"ROOT":0,"createTime":1519877965000,"showOrder":4,"updateTime":1519877966000,"id":24,"version":0,"parentId":5,"bodyPart":{"name":"臀部","value":"BUTTOCK"}}],"createTime":1519877206000,"showOrder":5,"updateTime":1519877207000,"id":5,"version":0,"parentId":0,"bodyPart":{"name":"下身","value":"LOWER_BODY"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519877997000,"showOrder":1,"updateTime":1519877998000,"id":25,"version":0,"parentId":6,"bodyPart":{"name":"左大臂","value":"LEFT_UPPER_ARM"}},{"ROOT":0,"createTime":1519878012000,"showOrder":2,"updateTime":1519878013000,"id":26,"version":0,"parentId":6,"bodyPart":{"name":"左小臂","value":"LEFT_LOWER_ARM"}},{"ROOT":0,"createTime":1519878030000,"showOrder":3,"updateTime":1519878031000,"id":27,"version":0,"parentId":6,"bodyPart":{"name":"左手肘","value":"LEFT_ELBOW"}}],"createTime":1519877235000,"showOrder":6,"updateTime":1519877237000,"id":6,"version":0,"parentId":0,"bodyPart":{"name":"左臂部","value":"LEFT_ARM"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878047000,"showOrder":1,"updateTime":1519878049000,"id":28,"version":0,"parentId":7,"bodyPart":{"name":"右大臂","value":"RIGHT_UPPER_ARM"}},{"ROOT":0,"createTime":1519878069000,"showOrder":2,"updateTime":1519878070000,"id":29,"version":0,"parentId":7,"bodyPart":{"name":"右小臂","value":"RIGHT_LOWER_ARM"}},{"ROOT":0,"createTime":1519878087000,"showOrder":3,"updateTime":1519878089000,"id":30,"version":0,"parentId":7,"bodyPart":{"name":"右手肘","value":"RIGHT_ELBOW"}}],"createTime":1519877279000,"showOrder":7,"updateTime":1519877280000,"id":7,"version":0,"parentId":0,"bodyPart":{"name":"右臂部","value":"RIGHT_ARM"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878124000,"showOrder":1,"updateTime":1519878125000,"id":31,"version":0,"parentId":8,"bodyPart":{"name":"左手腕","value":"LEFT_WRIST"}},{"ROOT":0,"createTime":1519878141000,"showOrder":2,"updateTime":1519878142000,"id":32,"version":0,"parentId":8,"bodyPart":{"name":"左手指","value":"LEFT_FINGER"}}],"createTime":1519877303000,"showOrder":8,"updateTime":1519877304000,"id":8,"version":0,"parentId":0,"bodyPart":{"name":"左手部","value":"LEFT_HAND"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878157000,"showOrder":1,"updateTime":1519878158000,"id":33,"version":0,"parentId":9,"bodyPart":{"name":"右手腕","value":"RIGHT_WRIST"}},{"ROOT":0,"createTime":1519878172000,"showOrder":2,"updateTime":1519878173000,"id":34,"version":0,"parentId":9,"bodyPart":{"name":"右手指","value":"RIGHT_FINGER"}}],"createTime":1519877316000,"showOrder":9,"updateTime":1519877316000,"id":9,"version":0,"parentId":0,"bodyPart":{"name":"右手部","value":"RIGHT_HAND"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878193000,"showOrder":1,"updateTime":1519878194000,"id":35,"version":0,"parentId":10,"bodyPart":{"name":"左大腿","value":"LEFT_THIGH"}},{"ROOT":0,"createTime":1519878207000,"showOrder":2,"updateTime":1519878208000,"id":36,"version":0,"parentId":10,"bodyPart":{"name":"左膝盖","value":"LEFT_KNEE"}},{"ROOT":0,"createTime":1519878223000,"showOrder":3,"updateTime":1519878224000,"id":37,"version":0,"parentId":10,"bodyPart":{"name":"左小腿","value":"LEFT_CALF"}}],"createTime":1519877361000,"showOrder":10,"updateTime":1519877361000,"id":10,"version":0,"parentId":0,"bodyPart":{"name":"左腿部","value":"LEFT_LEG"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878244000,"showOrder":1,"updateTime":1519878245000,"id":38,"version":0,"parentId":11,"bodyPart":{"name":"右大腿","value":"RIGHT_THIGH"}},{"ROOT":0,"createTime":1519878257000,"showOrder":2,"updateTime":1519878257000,"id":39,"version":0,"parentId":11,"bodyPart":{"name":"右膝盖","value":"RIGHT_KNEE"}},{"ROOT":0,"createTime":1519878272000,"showOrder":3,"updateTime":1519878274000,"id":40,"version":0,"parentId":11,"bodyPart":{"name":"右小腿","value":"RIGHT_CALF"}}],"createTime":1519877372000,"showOrder":11,"updateTime":1519877373000,"id":11,"version":0,"parentId":0,"bodyPart":{"name":"右腿部","value":"RIGHT_LEG"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878293000,"showOrder":1,"updateTime":1519878294000,"id":41,"version":0,"parentId":12,"bodyPart":{"name":"左脚踝","value":"LEFT_ANKLE"}},{"ROOT":0,"createTime":1519878304000,"showOrder":2,"updateTime":1519878305000,"id":42,"version":0,"parentId":12,"bodyPart":{"name":"左脚趾","value":"LEFT_TOE"}},{"ROOT":0,"createTime":1519878323000,"showOrder":3,"updateTime":1519878326000,"id":43,"version":0,"parentId":12,"bodyPart":{"name":"左跟腱","value":"LEFT_ACHILLES_TENDON"}}],"createTime":1519877398000,"showOrder":12,"updateTime":1519877398000,"id":12,"version":0,"parentId":0,"bodyPart":{"name":"左脚部","value":"LEFT_FOOT"}},{"ROOT":0,"children":[{"ROOT":0,"createTime":1519878341000,"showOrder":1,"updateTime":1519878344000,"id":44,"version":0,"parentId":13,"bodyPart":{"name":"右脚踝","value":"RIGHT_ANKLE"}},{"ROOT":0,"createTime":1519878363000,"showOrder":2,"updateTime":1519878364000,"id":45,"version":0,"parentId":13,"bodyPart":{"name":"右脚趾","value":"RIGHT_TOE"}},{"ROOT":0,"createTime":1519878374000,"showOrder":3,"updateTime":1519878375000,"id":46,"version":0,"parentId":13,"bodyPart":{"name":"右跟腱","value":"RIGHT_ACHILLES_TENDON"}}],"createTime":1519877419000,"showOrder":13,"updateTime":1519877420000,"id":13,"version":0,"parentId":0,"bodyPart":{"name":"右脚部","value":"RIGHT_FOOT"}}]
export const moodStatus = [{"name":"心情愉悦","value":"GOOD"},{"name":"心情一般","value":"NORMAL"},{"name":"心情糟糕","value":"BAD"}]
export const periodStatus = [{"name":"经期内","value":"IN"},{"name":"经期外","value":"OUT"}]
export const pulseType = [{"name":"晨脉","value":"MORNING"},{"name":"晚脉","value":"NIGHT"}]
export const resourceType = [{"name":"图片","value":"IMAGE"},{"name":"视频","value":"VIDEO"}]
export const teamLevel = [{"name":"一线","value":"FIRST"},{"name":"二线","value":"SECOND"}]
export const trainingContent = [{"code":"JISHU","createTime":1519529975000,"name":"技术","showOrder":1,"updateTime":1519529976000,"id":1,"version":0,"value":1},{"code":"SHENTISUZHI","createTime":1519529990000,"name":"身体素质","showOrder":2,"updateTime":1519529990000,"id":2,"version":0,"value":2},{"code":"ZHANSHU","createTime":1519530024000,"name":"战术","showOrder":3,"updateTime":1519530025000,"id":3,"version":0,"value":3},{"code":"ZONEHESHENTISUZHI","createTime":1519744328000,"name":"综合身体素质","showOrder":4,"updateTime":1519744329000,"id":4,"version":0,"value":4},{"code":"TIAOZHENG","createTime":1519744397000,"name":"调整","showOrder":5,"updateTime":1519744398000,"id":5,"version":0,"value":5}]
export const trainingIntensity = [{"name":"训练强度大","value":"BIG"},{"name":"训练强度中","value":"MIDDLE"},{"name":"训练强度小","value":"SMALL"}]
export const trainingRecoveryMethod = [{"code":"ZIRANHUIFU","createTime":1519530071000,"name":"自然恢复","showOrder":1,"updateTime":1519530072000,"id":1,"version":0,"value":1},{"code":"WULIHUIFU","createTime":1519530101000,"name":"物理恢复","showOrder":2,"updateTime":1519530102000,"id":2,"version":0,"value":2},{"code":"YINGYANGHUIFU","createTime":1519530151000,"name":"营养恢复","showOrder":3,"updateTime":1519530152000,"id":3,"version":0,"value":3},{"code":"XINLIHUIFU","createTime":1519530175000,"name":"心理恢复","showOrder":4,"updateTime":1519530176000,"id":4,"version":0,"value":4}]
export const trainingVolume = [{"name":"训练量大","value":"BIG"},{"name":"训练量中","value":"MIDDLE"},{"name":"训练量小","value":"SMALL"}]

export const PLACEHOLD_DICT = {
  bloodType,
  coachLevel,
  completeStatus,
  evaluationStatus,
  gender,
  injuryPart,
  moodStatus,
  periodStatus,
  pulseType,
  resourceType,
  teamLevel,
  trainingContent,
  trainingIntensity,
  trainingRecoveryMethod,
  trainingVolume
}

export const VERSION = 'v1.5'
export const OWNER = '王会立'
export const COPYRIGHT = `©${new Date().getFullYear()} 天津体育科学研究所  版权所有`
export const API_SERVER = 'http://47.104.150.10:8080'
