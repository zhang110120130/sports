/**
 * 简单的验证
 * 传入 rules 和 formData
 * 返回验证结果和错误信息
 * 
 * formData: {
 *    name: 'test',
 *    index: 1
 * }
 * 
 * rules: {
 *    name: {
 *      validator: (val) => {
 *         return val && val.trim()
 *      },
 *      message: '请填写名称'
 *    },
 *    index: {
 *      validator: (val) => {
 *        return val && /^\d+$/gi,
 *      },
 *      message: '请填写正确的索引值'
 *    }
 * }
 */

// helpers
const HELPERS = {
  required: (val) => {
    return val && val.trim()
  },
  num: (val) => {
    return val && val.trim() && /^\d+$/gi.test(val)
  },
  numList: (val) => {
    return val && val.trim() && /^(\d+,?)+\d+$/gi.test(val)
  },
  list: (val) => {
    return Array.isArray(val) && val.length > 0
  }
}

export default (formData, rules) => {
  let result = true
  let error = ''
  let key = ''

  for (let k in rules) {
    let rule = rules[k]
    if (rule) {
      let val = formData[k]
      let validator = rule.validator
      let _validator = typeof(validator) === 'string' ? HELPERS[validator] : validator
      let r = _validator(val)
      if (!r) {
        result = false
        error = rule.message
        key = k
        
        return [result, key, error]
      }
    }
  }

  return [result, key, error]
}

