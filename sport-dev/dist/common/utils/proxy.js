/**
 * proxy to api server
 */
import { API_SERVER } from 'common/constants/const'

let _API_SERVER = API_SERVER

if (process.env.NODE_ENV === 'development') {
  _API_SERVER = ''
}

export default (api_path) => {
  return `${_API_SERVER}${api_path}`
}