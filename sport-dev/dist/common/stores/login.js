import fetch from 'common/utils/fetch'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'common/constants/async-states'
import { API_LOGIN, API_LOGIN_BACKGROUND } from 'common/constants/api'
import { StorageMaster } from 'common/utils/storage'
import { LOGIN, LOGIN_FAIL, LOGIN_SUCCESS,
         LOAD_LOGIN_BACKGROUND, LOAD_LOGIN_BACKGROUND_FAIL, 
         LOAD_LOGIN_BACKGROUND_SUCCESS } from 'common/constants/action-types'
import createGetters from 'common/utils/create-getters'


export const state = {
  loadLoginBackgroundStataus: DEFAULT,
  loginBackground: null,
  loginStatus: DEFAULT,
  loginMessage: '',
  user: null
}

export const getters = createGetters(state)

export const actions = {
  loadLoginBackground ({commit, state}, data) {
    commit(LOAD_LOGIN_BACKGROUND)
    const setting = StorageMaster.getItem('setting')
    fetch.get(API_LOGIN_BACKGROUND, null, {role: setting.role}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOAD_LOGIN_BACKGROUND_SUCCESS, res.data)
      } else {
        commit(LOAD_LOGIN_BACKGROUND_FAIL, res.message)
      }
    }).catch((err) => {
      console.error(err)
    })
  },
  login ({commit, state}, data) {
    commit(LOGIN, data)
    const setting = StorageMaster.getItem('setting')
    fetch.post(API_LOGIN, data, {role: setting.role}).then((res) => {
      if (parseInt(res.code, 10) === 1) {
        commit(LOGIN_SUCCESS, res.data)
      } else {
        let message = '登录失败'
        if (res.code === 404) {
          message = '该用户不存在'
        }
        commit(LOGIN_FAIL, message)
      }
    }).catch((err) => {
      console.error(err)
      commit(LOGIN_FAIL, '网络错误')
    })
  }
}

export const mutations = {
  [LOAD_LOGIN_BACKGROUND] (state) {
    state.loadLoginBackgroundStataus = PENDING
  },
  [LOAD_LOGIN_BACKGROUND_FAIL] (state) {
    state.loadLoginBackgroundStataus = FAIL
  },
  [LOAD_LOGIN_BACKGROUND_SUCCESS] (state, data) {
    state.loadLoginBackgroundStataus = SUCCESS
    state.loginBackground = data
  },
  [LOGIN] (state, data) {
    state.loginStatus = PENDING
    state.loginMessage = ''
  },
  [LOGIN_FAIL] (state, message) {
    state.loginStatus = FAIL
    state.loginMessage = message
  },
  [LOGIN_SUCCESS] (state, data) {
    state.loginStatus = SUCCESS
    state.user = data
    StorageMaster.setItem('user', data)
    StorageMaster.setItem('loginName', data.current_account)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}